using Opacha.Profit.Extensions.Coroutines;
using Opacha.Profit.Extensions.Randoms;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class EnemiesSpawner : Spawner
    {
        [SerializeField] private float spawnInterval;
        [SerializeField] private float range;

        private int spawnActionIdx;
        
        private void Start() =>
            spawnActionIdx = this.RepetitiveAction(() => Spawn(RandomExtensions.RandomPosition(range)), spawnInterval);

        private void OnDestroy() => this.TerminateAction(spawnActionIdx);
    }
}