using OpachaTemplate.Core;
using OpachaTemplate.Core.Audio;
using OpachaTemplate.Core.Audio.Data;
using UnityEngine;

namespace OpachaTemplate.Demo
{
    public class DemoAudioManager : MonoSingleton<DemoAudioManager>
    {
        [SerializeField] private AudioManager2D audioManager;

        [SerializeField] private SoundData hitSound;

        public void PlayHit() => audioManager.PlaySound(hitSound);
    }
}