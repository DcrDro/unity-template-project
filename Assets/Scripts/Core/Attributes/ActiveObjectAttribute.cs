﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ActiveObjectAttribute : PropertyAttribute
    {
    }
}
