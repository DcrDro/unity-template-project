﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ShowIfAttribute : PropertyAttribute
    {
        public string ConditionPropertyName { get; }
        public int PopupValue { get; } = -1;

        public ShowIfAttribute(string conditionBoolPropertyName)
        {
            this.ConditionPropertyName = conditionBoolPropertyName;
        }
        
        // this is to maintain checking Popup values
        public ShowIfAttribute(string conditionIntPropertyName, int popupValue)
        {
            this.ConditionPropertyName = conditionIntPropertyName;
            PopupValue = popupValue;
        }
    }
}
