﻿using UnityEngine;
using UnityEditor;
using OpachaTemplate.Core.Editor.Menus;

namespace OpachaTemplate.Core.Editor.Windows
{
    /*
     * TODO:
     *  - добавить и разнообразить верстку
     */  

    public class ToolsWindow : EditorWindow
    {
        [MenuItem(CC.TOOLS_MENU + "/" + CC.TOOLS_WINDOW)]
        static void Init()
        {
            ToolsWindow window = EditorWindow.GetWindow(typeof(ToolsWindow)) as ToolsWindow;
            window.titleContent.text = CC.TOOLS_WINDOW;
            window.Show();
        }

        private void OnGUI()
        {
            var windowHeader = WindowHeaderLabelStyle();
            var categoryHeader = CategoryHeaderLabelStyle();
            var note = NoteLabelStyle();
            
            GUILayout.Space(25);
            GUILayout.Label("Opacha tools window", windowHeader);
            
            GUILayout.Space(50);
            GUILayout.Label("Here will be useful tools for development", note);
            
            GUILayout.Space(50);
            GUILayout.Label("Storage tools", categoryHeader);
            if (GUILayout.Button(CC.CLEAR_STORAGE, GUILayout.Width(150)))
            {
                StorageClearer.ClearStorage();
            }
            
            GUILayout.Space(50);
            GUILayout.Label("Resolution tools", categoryHeader);
            if (GUILayout.Button(CC.RESOLUTION_HOR, GUILayout.Width(250)))
            {
                ResolutionSwitcher.SwitchHorizontal();
            }
            if (GUILayout.Button(CC.RESOLUTION_VER, GUILayout.Width(250)))
            {
                ResolutionSwitcher.SwitchVertical();
            }
        }

        private GUIStyle WindowHeaderLabelStyle()
        {
            GUIStyle style = new GUIStyle();
            style.fontSize = 30;
            style.alignment = TextAnchor.MiddleCenter;
            style.fontStyle = FontStyle.Bold;

            return style;
        }

        private GUIStyle CategoryHeaderLabelStyle()
        {
            GUIStyle style = new GUIStyle();
            style.fontSize = 24;
            style.alignment = TextAnchor.MiddleLeft;
            style.fontStyle = FontStyle.Italic;

            return style;
        }
        
        private GUIStyle NoteLabelStyle()
        {
            GUIStyle style = new GUIStyle();
            style.fontSize = 18;
            style.alignment = TextAnchor.MiddleCenter;
            style.fontStyle = FontStyle.Italic;

            return style;
        }

    }
}