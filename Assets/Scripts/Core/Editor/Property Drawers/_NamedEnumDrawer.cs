﻿using UnityEngine;
using UnityEditor;
using OpachaTemplate.Core.Attributes;
using System;

namespace OpachaTemplate.Core.Editor.Properties
{
    /* (Находится в разработке)
     * Работает с ограничемями:
     *  - Поле, с которым он работает, должно быть такого типа, который явялется "оберткой" над массивом элементов нужного типа
     *  - массив имеет имя'objects'
     *  - этот тип не должен быть генерик
     */ 

    [CustomPropertyDrawer(typeof(NamedEnumAttribute), true)]
    public class _NamedEnumDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var attributes = fieldInfo.GetCustomAttributes(typeof(NamedEnumAttribute), true);
            NamedEnumAttribute attr = attributes[0] as NamedEnumAttribute;
            var enumNames = Enum.GetNames(attr.enumType);

            var arrayProperty = property.FindPropertyRelative("objects");
            EditorGUI.BeginProperty(position, label, property);
            Rect pos = position;
            pos.height = GetElementHeight(arrayProperty);
            EditorGUI.PropertyField(pos, arrayProperty);

            if (arrayProperty.isExpanded)
            {
                var height= GetElementHeight(arrayProperty);
                Vector2 elementSize = new Vector2(position.width, height);
                Rect rect = new Rect(position.position, elementSize);

                for (int i = 0; i < arrayProperty.arraySize; i++)
                {
                    rect.y += height;
                    SerializedProperty element = arrayProperty.GetArrayElementAtIndex(i);
                    GUIContent elementLabel = new GUIContent(GetLabel(enumNames, i));
                    EditorGUI.PropertyField(rect, element, elementLabel);
                }
            }

            EditorGUI.EndProperty();

            if (GUILayout.Button("clear"))
            {
                arrayProperty.ClearArray();
            }
        }

        private static float GetElementHeight(SerializedProperty arrayProperty)
        {
            if (arrayProperty.arraySize == 0) return 25; // !

            SerializedProperty element = arrayProperty.GetArrayElementAtIndex(0);
            return EditorGUI.GetPropertyHeight(element);
        }

        private static string GetLabel(string[] names, int i) => i < names.Length ? names[i] : "unknown";

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty serializedProperty = property.FindPropertyRelative("objects");
            float propHeight = EditorGUI.GetPropertyHeight(serializedProperty);

            return base.GetPropertyHeight(property, label) + (serializedProperty.isExpanded ? propHeight : 0);
        }
    }
}
