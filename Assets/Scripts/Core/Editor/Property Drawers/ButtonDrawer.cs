﻿using System.Reflection;
using UnityEngine;
using UnityEditor;
using OpachaTemplate.Core.Attributes;

namespace OpachaTemplate.Core.Editor.Properties
{
    [CustomPropertyDrawer(typeof(ButtonAttribute), true)]
    public class ButtonDrawer : PropertyDrawer
    {
        private ButtonAttribute ButtonAttribute => (ButtonAttribute) attribute;
        private object Target(SerializedProperty property) => property.serializedObject.targetObject;
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            if (GUI.Button(position, ButtonAttribute.MethodName))
            {
                var method = Target(property).GetType().GetMethod(ButtonAttribute.MethodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                method?.Invoke(Target(property), null);
            }
            EditorGUI.EndProperty();
        }
    }
}
