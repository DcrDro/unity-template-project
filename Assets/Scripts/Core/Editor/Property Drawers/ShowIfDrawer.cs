﻿using OpachaTemplate.Core.Attributes;
using UnityEngine;
using UnityEditor;

namespace OpachaTemplate.Core.Editor.Properties
{
    [CustomPropertyDrawer(typeof(ShowIfAttribute), true)]
    public class ShowIfDrawer : PropertyDrawer
    {
        private ShowIfAttribute ShowIfAttribute => (ShowIfAttribute) attribute;
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (CanShowProperty(property))
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }

        private bool CanShowProperty(SerializedProperty property)
        {
            SerializedProperty conditionalProperty =
                property.serializedObject.FindProperty(ShowIfAttribute.ConditionPropertyName);
            return conditionalProperty != null && ShowIfAttribute.PopupValue == -1
                ? conditionalProperty.boolValue
                : conditionalProperty.intValue == ShowIfAttribute.PopupValue;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) =>
            CanShowProperty(property) ? EditorGUI.GetPropertyHeight(property, label, true) : 0;
    }
}
