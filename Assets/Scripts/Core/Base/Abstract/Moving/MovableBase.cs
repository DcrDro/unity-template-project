﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public abstract class MovableBase : MonoBehaviour, IMovable
    {
        public abstract Vector3 Velocity { get; }
        public abstract Vector3 Position { get; }
    }
}
