﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public abstract class ForceMovableBase : MovableBase, IForceMovable
    {
        [SerializeField] protected float forceSpeed;

        public abstract void MoveWith(Vector3 direction);
    }
}
