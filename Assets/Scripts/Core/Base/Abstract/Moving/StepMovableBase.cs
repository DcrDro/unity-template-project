﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public abstract class StepMovableBase : MovableBase
    {
        [SerializeField] protected float moveSpeed;

        public abstract void MoveBy(Vector3 step);
    }
}
