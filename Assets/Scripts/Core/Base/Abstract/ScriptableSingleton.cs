﻿using UnityEngine;

namespace OpachaTemplate.Core
{
    public abstract class ScriptableSingleton<T> : ScriptableObject where T : ScriptableObject
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    var objects = Resources.FindObjectsOfTypeAll<T>();
                    if (objects.Length > 0)
                    {
                        _instance = objects[0];
                    }
                }

                return _instance;
            }
        }
    }
}
