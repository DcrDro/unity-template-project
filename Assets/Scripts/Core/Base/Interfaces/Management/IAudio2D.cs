﻿using OpachaTemplate.Core.Audio.Data;

namespace OpachaTemplate.Core.Audio
{
    public interface IAudio2D
    {
        void PlayMusic(MusicData music);
        void PauseMusic();
        void UnpauseMusic();
        void StopMusic();
        
        void SetMusicVolume(float volume);
        
        void PlaySound(SoundData sound);
        void StopSound();
    }
}