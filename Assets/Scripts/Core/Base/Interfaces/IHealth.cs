﻿using System;

namespace OpachaTemplate.Core.Combat
{
    public interface IHealth
    {
        float HealthAmount { get; }
        float MaxHealthAmount { get; }
        void UpdateHealth(float amount);

        event Action OnHealthUpdated;
        event Action OnDied;
    }
}