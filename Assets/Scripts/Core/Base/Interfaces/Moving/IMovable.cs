﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public interface IMovable : IPositionable
    {
        Vector3 Velocity { get; }
    }
}
