﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public interface IPositionable
    {
        Vector3 Position { get; }
    }
}
