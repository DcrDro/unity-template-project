﻿using OpachaTemplate.Core.Management;
using UnityEngine;

namespace OpachaTemplate.Core.UI
{
    public class FinalMenuPanelManager : MonoSingleton<FinalMenuPanelManager>
    {
        [SerializeField] private PanelActivator panelActivator;

        // -- UI Events --

        public void OnMenu() => ApplicationManager.Instance.OpenMainMenu();

        public void OnExit() => ApplicationManager.Instance.ExitGame();
    }
}