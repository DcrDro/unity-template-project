﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace OpachaTemplate.Core.UI
{
    [RequireComponent(typeof(Image))]
    public class ImageScaler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private float pressedScale = 0.9f;

        public void OnPointerDown(PointerEventData eventData) => transform.localScale = Vector3.one * pressedScale;
        public void OnPointerUp(PointerEventData eventData) => transform.localScale = Vector3.one;
    }
}