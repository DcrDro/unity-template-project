﻿using UnityRandom = UnityEngine.Random;

namespace OpachaTemplate.Core.Extensions.Randoms
{
    public static class RandomExtensions
    {
        public static float Random(this Diapason diapason) => UnityRandom.Range(diapason.MinValue, diapason.MaxValue);
    }
}
