﻿using OpachaTemplate.Core.UI;

namespace OpachaTemplate.Core.Extensions.UIs
{
    public static class UIExtensions
    {
        // TODO: show/hide via activator
        public static void ShowOnly(this PanelActivator activator, UIPanel[] allPanels, UIPanel panelToShow)
        {
            foreach (var panel in allPanels)
            {
                activator.DeactivatePanel(panel);
            }
            activator.ActivatePanel(panelToShow);
        }

        public static void HideAll(this PanelActivator activator, UIPanel[] allPanels)
        {
            foreach (var panel in allPanels)
            {
                activator.DeactivatePanel(panel);
            }
        }
    }
}