﻿using System;
using UnityEngine;

namespace OpachaTemplate.Core
{
    [Serializable]
    public class Diapason
    {
        [SerializeField] private float minValue;
        [SerializeField] private float maxValue;

        public float MinValue => minValue;
        public float MaxValue => maxValue;
    }
}
