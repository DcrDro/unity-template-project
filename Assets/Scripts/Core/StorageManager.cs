﻿using OpachaTemplate.Core.Attributes;
using UnityEngine;

namespace OpachaTemplate.Core.Management
{
    public class StorageManager : MonoSingleton<StorageManager>, IStorage
    {
        public void Save(string key, int value) => PlayerPrefs.SetInt(key, value);
        public void Save(string key, float value) => PlayerPrefs.SetFloat(key, value);
        public void Save(string key, string value) => PlayerPrefs.SetString(key, value);

        public int LoadInt(string key) => PlayerPrefs.GetInt(key);
        public float LoadFloat(string key) => PlayerPrefs.GetFloat(key);
        public string LoadString(string key) => PlayerPrefs.GetString(key);

        public void ClearAll() => PlayerPrefs.DeleteAll();

        [SerializeField, Button(nameof(ClearAll))]
        private byte _clearButton;
    }
}