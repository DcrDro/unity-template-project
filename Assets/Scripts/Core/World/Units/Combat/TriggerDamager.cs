﻿using UnityEngine;

namespace OpachaTemplate.Core.Combat
{
    public class TriggerDamager : MonoBehaviour
    {
        [SerializeField] private Damager damager;
        [SerializeField] private TriggerSender2D trigger;

        private void OnEnable()
        {
            damager.OnDidDamage += OnDidDamage;
            trigger.OnEnter += OnEnter;
        }

        private void OnDisable()
        {
            damager.OnDidDamage -= OnDidDamage;
            trigger.OnEnter -= OnEnter;
        }

        private void OnDidDamage(float damage)
        {
            damage.Log("did damage");
        }

        private void OnEnter(Collider2D other)
        {
            if (other.TryGetComponent<IRootReference>(out var root))
            {
                IHealth health = root.RootObject.GetComponentInChildren<IHealth>();
                if (health != null)
                {
                    damager.DoDamage(health);
                }
            }
        }
    }
}
