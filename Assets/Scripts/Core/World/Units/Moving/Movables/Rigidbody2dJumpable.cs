﻿using UnityEngine;

namespace OpachaTemplate.Core.Units
{
    public class Rigidbody2dJumpable : ForceMovableBase
    {
        [SerializeField] private Rigidbody2D rigidbody2d;

        public override Vector3 Velocity => rigidbody2d.velocity;

        public override Vector3 Position => rigidbody2d.position;

        public override void MoveWith(Vector3 direction)
        {
            rigidbody2d.AddForce(direction * forceSpeed);
        }
    }
}
