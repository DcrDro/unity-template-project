﻿using OpachaTemplate.Core;
using OpachaTemplate.Core.UI;
using System.Collections;
using UnityEngine;

namespace OpachaTemplate.Game.UI
{
    public class MainMenuUIManager : MonoSingleton<MainMenuUIManager>
    {
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(0.5f);
            MainMenuPanelManager.Instance.ShowGameTitlePopup();
            yield return new WaitForSeconds(0.5f);
            MainMenuPanelManager.Instance.ShowPlayGameButtonPopup();
            MainMenuPanelManager.Instance.ShowDemoButtonPopup();
            MainMenuPanelManager.Instance.ShowExitButtonPopup();
        }
    }
}
